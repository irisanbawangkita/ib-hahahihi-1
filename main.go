package main

import (
	"fmt"
	"net/http"
)

const (
	nama = "Ziath gantenk"
	umur = "19"
)

func main() {
	http.HandleFunc("/hello", handlerHello)
	http.HandleFunc("/yoga", YogaTampan)
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		keys, _ := r.URL.Query()["a"]
		fmt.Fprintf(w, "Hello %s!", keys[0])
	})

	http.HandleFunc("/riana", PunyaRiana)

	var address = "localhost:9000"
	fmt.Printf("server started at %s\n", address)
	http.HandleFunc("/angga", HandlerSameQuery)

	err := http.ListenAndServe(address, nil)
	if err != nil {
		fmt.Println(err.Error())
	}
}

func handlerHello(w http.ResponseWriter, r *http.Request) {
	var message = "Hello world!"
	w.Write([]byte(message))
}
